#!/bin/bash

################################################################################
# script to set up user-specific ros environment variables. This should be kept
# as /home/root/ros_env.sh on the robot and should be called by .bashrc
# so that it is loaded when bash starts.
#
# author: james@modalai.com
################################################################################


# Set ROS_IP & ROS_MASTER_URI appropriately for your configuration
# 192.168.8.1 is default for the robot in soft access point mode
export ROS_MASTER_URI=http://localhost:11311/
export ROS_IP=192.168.8.1
unset ROS_HOSTNAME