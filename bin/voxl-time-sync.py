#!/usr/bin/python

import sys
import time
import datetime

def getYear():
    year = datetime.datetime.now().year
    print 'Current year: %d' % year
    return year

year = getYear()
while year < 2020:
    time.sleep(1)
    year = getYear()

sys.exit(0)

